class patterns {
    public static void main(String[] args) {
        int size = 5 ;
        pattern(size) ;
    }
    
    public static void pattern (int size){
    
    int row ;
    int col ;
    int num = 1 ;
    
    //ascending triangle
    for ( row = 0 ; row < size ; row++) {
        System.out.println(" ");
        num = 1 ;
        
        for ( col = 0 ; col < row ; col ++){
            System.out.print(num + " ");
            num++ ;
        }
    }
    //Returning triangle
     for ( row = 0 ; row < size ; row++) {
        System.out.println(" ");
        num = 1 ;
        
        for ( col = size ; col > row ; col--){
            System.out.print(num + " ");
               num++ ;
        }
    }
    
    }
} 